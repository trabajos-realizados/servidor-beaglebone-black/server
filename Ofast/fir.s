	.arch armv7-a
	.eabi_attribute 28, 1
	.eabi_attribute 23, 1
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.file	"fir.c"
	.text
	.align	1
	.p2align 2,,3
	.global	filtro_fir
	.arch armv7-a
	.syntax unified
	.thumb
	.thumb_func
	.fpu vfpv3-d16
	.type	filtro_fir, %function
filtro_fir:
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r3, #0
	push	{r4, r5, r6, r7, r8, r9, lr}
	ldrd	r6, r5, [sp, #28]
	ble	.L1
	add	lr, r6, #-1
	subs	r7, r3, #1
	mov	r3, #-1
	add	r8, r1, lr, lsl #2
	b	.L27
.L37:
    vldr.32	s13, [r5, #4]
	cmp	r3, #1
	mov	r9, #2
	mov	r4, #1
	vldr.32	s14, [r0, #-8]
	vmla.f32	s15, s13, s14
	bmi	.L5
.L38:
	vldr.32	s13, [r5, #8]
	vldr.32	s14, [r0, #-12]
	vmla.f32	s15, s13, s14     
.L6:
	cmp	r3, #2
	bmi	.L7
.L39:
	vldr.32	s13, [r5, #12]
	vldr.32	s14, [r0, #-16]
	vmla.f32	s15, s13, s14
.L8:
	cmp	r3, #3
	bmi	.L9
.L40:
    vldr.32	s13, [r5, #16]
	vldr.32	s14, [r0, #-20]
	vmla.f32	s15, s13, s14
.L10:
	cmp	r3, #4
	bmi	.L11
.L41:
	vldr.32	s13, [r5, #20]
	vldr.32	s14, [r0, #-24]
	vmla.f32	s15, s13, s14
.L12:
	cmp	r3, #5
	bmi	.L13
.L42:
	vldr.32	s13, [r5, #24]
	cmp	r3, #6
	vldr.32	s14, [r0, #-28]
	vmla.f32	s15, s13, s14
	bmi	.L16
.L32:
	vldr.32	s13, [r5, #28]
	cmp	r3, #7
	vldr.32	s14, [r0, #-32]
	vmla.f32	s15, s13, s14
	bmi	.L19
.L30:
	vldr.32	s13, [r5, #32]
	cmp	r3, #8
	vldr.32	s14, [r0, #-36]
	vmla.f32	s15, s13, s14
	bmi	.L22
.L31:
	vldr.32	s13, [r5, #36]
	cmp	r3, #9
	vldr.32	s14, [r0, #-40]
	vmla.f32	s15, s13, s14
	bmi	.L25
.L29:
	vldr.32	s13, [r5, #40]
	vldr.32	s14, [r0, #-44]
	vmla.f32	s15, s13, s14
.L26:
    adds	r3, r3, #1
	vstmia.32	r2!, {s15}
	cmp	r3, r7
	beq	.L1
.L27:
	vldmia.32	r0!, {s14}
	cmp	r3, #0
	vldr.32	s15, [r5]
	vmul.f32	s15, s14, s15
	bge	.L37
	cmp	lr, #0
	ittte	ge
	vldrge.32	s13, [r5, #4]
	movge	r9, #3
	movge	r4, #2
	movlt	r9, #2
	itee	lt
	movlt	r4, #1
	vldrge.32	s14, [r8]
	vmlage.f32	s15, s13, s14
	cmp	r3, #1
	bpl	.L38
.L5:
	subs	ip, r6, r4
	bmi	.L6
	add	ip, r1, ip, lsl #2
	vldr.32	s14, [r5, #8]
	cmp	r3, #2
	mov	r4, r9
	vldr.32	s13, [ip]
	vmla.f32	s15, s13, s14
	bpl	.L39
.L7:
	subs	ip, r6, r4
	bmi	.L8
	add	ip, r1, ip, lsl #2
	vldr.32	s14, [r5, #12]
	cmp	r3, #3
	add	r4, r4, #1
	vldr.32	s13, [ip]
	vmla.f32	s15, s13, s14
	bpl	.L40
.L9:
    subs	ip, r6, r4
	bmi	.L10
	add	ip, r1, ip, lsl #2
	vldr.32	s14, [r5, #16]
	cmp	r3, #4
	add	r4, r4, #1
	vldr.32	s13, [ip]
	vmla.f32	s15, s13, s14
	bpl	.L41
.L11:
	subs	ip, r6, r4
	bmi	.L12
	add	ip, r1, ip, lsl #2
	vldr.32	s14, [r5, #20]
	cmp	r3, #5
	add	r4, r4, #1
	vldr.32	s13, [ip]
	vmla.f32	s15, s13, s14
	bpl	.L42
.L13:
	subs	ip, r6, r4
	bmi	.L15
	add	ip, r1, ip, lsl #2
	vldr.32	s14, [r5, #24]
	cmp	r3, #6
	add	r4, r4, #1
	vldr.32	s13, [ip]
	vmla.f32	s15, s13, s14
	bpl	.L32
.L16:
	subs	ip, r6, r4
	bmi	.L18
	add	ip, r1, ip, lsl #2
	vldr.32	s14, [r5, #28]
	cmp	r3, #7
	add	r4, r4, #1
	vldr.32	s13, [ip]
	vmla.f32	s15, s13, s14
	bpl	.L30
.L19:
	subs	ip, r6, r4
	bmi	.L21
	add	ip, r1, ip, lsl #2
	vldr.32	s14, [r5, #32]
	cmp	r3, #8
	add	r4, r4, #1
	vldr.32	s13, [ip]
	vmla.f32	s15, s13, s14
	bpl	.L31
.L22:
    subs	ip, r6, r4
	bmi	.L24
	add	ip, r1, ip, lsl #2
	vldr.32	s14, [r5, #36]
	cmp	r3, #9
	add	r4, r4, #1
	vldr.32	s13, [ip]
	vmla.f32	s15, s13, s14
	bpl	.L29
.L25:
	subs	r4, r6, r4
	add	r3, r3, #1
	itttt	pl
	addpl	r4, r1, r4, lsl #2
	vldrpl.32	s14, [r5, #40]
	vldrpl.32	s13, [r4]
	vmlapl.f32	s15, s13, s14
	cmp	r3, r7
	vstmia.32	r2!, {s15}
	bne	.L27
.L1:
	pop	{r4, r5, r6, r7, r8, r9, pc}
.L15:
	cmp	r3, #6
	bpl	.L32
.L18:
	cmp	r3, #7
	bpl	.L30
.L21:
	cmp	r3, #8
	bpl	.L31
.L24:
    cmp	r3, #9
	bpl	.L29
	b	.L26
	.size	filtro_fir, .-filtro_fir
	.ident	"GCC: (Debian 8.3.0-6) 8.3.0"
	.section	.note.GNU-stack,"",%progbits

