/* Este codigo es realizado con el fin de verificar la funcionalidad del Item del 2do Recuperatorio 
 * 
 * Este proceso intentara abrir el dispositivo I2C.
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
       
int main()
{
    printf("Proceso de prueba. Se intentara abrir el dispositivo I2C.\n");
    
    int fd_i2c;

    printf("Abriendo i2c_td3\n");
    fd_i2c = open("/dev/i2c_td3", O_RDWR);
    if( fd_i2c < 0 )
    {
        printf("Error open i2c\n");
        return -1;
    }
    
    printf("Se logro abrir el dispositivo i2c_td3!!\n");
    
    printf("Cerrando dispositivo\n");
    
    close(fd_i2c);
    
    printf("Fin del programa de prueba!\n");
    
    return 0;
}
