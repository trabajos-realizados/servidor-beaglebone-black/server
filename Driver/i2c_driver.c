/* Pendientes:
 *
 * Notas:
 *      1) Limitado a enviar 32B
 *      2) Limitado a recibir 1024B
 *
 *      3) flag_BF: Bus Free, cuando esta en 1 indica que el bus esta libre luego de un transferencia. Esto se da cuanto se detecta el flanco de STOP.
 *      4) ARDY: indica que una transfercia ha finalizado
 *          Modo TX: STP = 1, DCOUNT = 0
 *                   STP = 0. DCOUNT = 0
 *          Modo Rx: STP = 1, DCOUNT = 0, FIFO RX = empty
 *                   STP = 0, DCOUNT = 0, FIFO RX = empty
 *      6) RDR: Receive draining, cuando se recibe STP y aun hay datos en la FIFO RX, el cual no han alcanzado el nivel de umbral.
 *      5) Error code: https://mariadb.com/kb/en/operating-system-error-codes/
 */

#include "i2c_driver.h"

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Alanes Carlos");
MODULE_VERSION("1.0");
MODULE_DESCRIPTION("Driver I2C: Acelerometro");

/****************************************************
 *  Variables globales.                             *
 ****************************************************/
static volatile uint8_t *flag_ARDY, *flag_BF, *dato;
static volatile uint16_t  *offset_rx;
static volatile uint8_t flag_I2C=0; //Esta bandera indica si un proceso ha abierto el dispositivo.

static wait_queue_head_t i2c_waitqueue;

static dev_t i2c_dev;
static struct cdev *i2c_p_cdev;
static struct class *i2c_pclase;
static struct device *i2c_pdevice_sys;

static struct
{
    void *control_module, *cm_per, *i2c2_reg;
    int IRQ_BB, int_i2c;
}HostCtrl;

static const struct of_device_id Mis_dispositivos_compatibles [] = {
    { .compatible = COMPATIBLE_NAME },
    { /* End of List */ },
};

MODULE_DEVICE_TABLE(of, Mis_dispositivos_compatibles);

/* Función platform driver */
static struct platform_driver Mi_I2C_Host_Controller = {
    .probe = probe_function,
    .remove = probe_remove,
    .driver = {
        .name = DRIVER_NAME,
        .of_match_table = of_match_ptr(Mis_dispositivos_compatibles),
    },
};

static struct file_operations i2c_ops =
{
    .owner = THIS_MODULE,
    .unlocked_ioctl = i2c_ioctl,
    .open = i2c_open,
    .release = i2c_close,
    .read = i2c_read,
    .write = i2c_write
};

/****************************************************
 *  File Operations                                 * 
 ****************************************************/
static long i2c_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{    
    switch(cmd)
    {
        case I2C_SLAVE:     //Slave Addres configuration
            printk(KERN_NOTICE"ioctl: Slave Address configuration = %02X.\n", (uint8_t)arg);
            iowrite32((uint32_t)arg, HostCtrl.i2c2_reg + I2C_SA);       //Escribo dirección destino.
            break;
            
        case I2C_STOP:      //Stop condition
            //printk(KERN_NOTICE"ioctl: Stop i2c.\n");
            iowrite32(I2C_EN|MST|TRX|STP, HostCtrl.i2c2_reg + I2C_CON);
            if( wait_event_interruptible(i2c_waitqueue, (*flag_BF==1) ) !=0 )
            {
                printk(KERN_ALERT"Read error: wait_event_interruptible() interrumpido por una signal. No se cumple ARDY = 1.\n");
                return -ERESTARTSYS;
            }
            break;
            
        default:
            printk(KERN_ALERT"ioctl: parameters error\n");
            return -EINVAL;
    }
    
    return 0;   //Si todo fue exitoso.
}

static int i2c_open(struct inode *inode, struct file *file)
{
    //Consulto si otro proceso ha abierto el dispositivo.
    if( flag_I2C == 1 )
    {
        printk(KERN_ALERT "Open: el dispositivo I2C se encuentra abierto por otro proceso\n.");
        printk(KERN_ALERT "Se activara el modo Sleep hasta que se libere.\n");
        
        //Se espera hasta que el dispositivo se libere.
        while( flag_I2C==1 )
        {
            udelay(1000);
        }
    }
    
    //Indico que el proceso actual ha tomado el dispositivo
    flag_I2C = 1;
    
    printk(KERN_NOTICE "Open: init driver I2C.\n");
    
    //Flags de interrupciones
    flag_ARDY = (uint8_t*)kmalloc(1, GFP_ATOMIC);
    flag_BF = (uint8_t*)kmalloc(1, GFP_ATOMIC);
    dato = (uint8_t*)kmalloc(1024, GFP_ATOMIC);     //65536 bytes es lo maximo que permite el dispositivo en transmision.
    offset_rx = (uint16_t*)kmalloc(2, GFP_ATOMIC);
    
    if( (flag_ARDY==NULL) || (flag_BF==NULL) || (dato==NULL) || (offset_rx==NULL) )
    {
        printk(KERN_ALERT"Error open: kmalloc(). No se pudo reservar memoria.\n");
        return -ENOMEM;
    }
    
    //Inicializo valores
    *flag_ARDY=0;
    
    ioread32(HostCtrl.i2c2_reg + I2C_SYSS);
    
    printk(KERN_NOTICE "Open: successfully completed.\n");
    
    return 0;
}

static int i2c_close(struct inode *inode, struct file * file)
{
    printk(KERN_NOTICE"Close: remove driver I2C.\n");
    //Libero recursos
    kfree((const void*)flag_ARDY);
    kfree((const void*)flag_BF);
    kfree((const void*)dato);
    kfree((const void*)offset_rx);
    
    flag_I2C = 0;   //Indico que se ha liberado el dispositivo
    
    return 0;
}

static ssize_t i2c_write(struct file *file, const char __user *userbuff, size_t size_usr, loff_t *offset)
{
    static uint32_t cont=0;
    
    //printk(KERN_NOTICE"write: data transmission\n");
    
    //Verifico que en el buffer no este vacio
    if( size_usr < 1 )
    {
        printk(KERN_ALERT "Write Error: user buffer vacio\n");
        return -EINVAL;
    }
    
    if( size_usr > 32 )
    {
        printk("Write Error: user buffer mas de 32 bytes\n");
        return -EINVAL;
    }
    
    //Copio los datos del usuario. Esta funcion a su vez, realiza la verificacion del puntero.
    if(copy_from_user((char *)dato, userbuff, size_usr) > 0)
    {
        printk(KERN_ALERT "Write Error: Fallo copy_from_user.\n");
        return -EFAULT;
    }
    
    *flag_ARDY = 0; //Se limpia el flag, "acceso listo".
    *flag_BF = 0;   //Se limpia el flag de bus free.
    
    //Cargo los bytes a transmitir en al FIFO
    iowrite32(size_usr, HostCtrl.i2c2_reg + I2C_CNT);       //Cantidad de datos transmitir
    for(cont=0; cont < size_usr; cont++)
    {
        iowrite8(*(dato + cont), HostCtrl.i2c2_reg + I2C_DATA); //Cargo en la FIFO el dato
    }
    
    //Inicio transferencia: MST, TX, START
    iowrite32(I2C_EN|MST|TRX|STT, HostCtrl.i2c2_reg + I2C_CON);
    //printk(KERN_NOTICE"Write: transmission started\n");

    //Modo sleep hasta que termine la transmición.
    if( wait_event_interruptible(i2c_waitqueue, (*flag_ARDY==1) ) != 0 )
    {
        printk(KERN_ALERT"Write Error: wait_event_interruptible(), interrumpido por una signal. No se cumple ARDY=1.\n");
        return -ERESTARTSYS;
    }
    
    //printk(KERN_NOTICE "Write: successfully completed\n");
    *flag_ARDY = 0;     //Limpio flag de "acceso libre".

    return size_usr;
}

static ssize_t i2c_read(struct file *file, char __user *userbuff, size_t size_usr, loff_t *offset)
{
    //printk(KERN_NOTICE "Read: data reception.\n");
    
    if(size_usr < 1)
    {
        printk(KERN_ALERT "Read error: userbuff vacio.\n");
        return -EINVAL;
    }
    if(size_usr > 1024)
    {
        printk(KERN_ALERT "Read error: userbuff debe ser menor a 1024B.\n");
        return -EINVAL;
    }
    if( access_ok(VERIFY_WRITE, userbuff, size_usr) == 0 )  //Se verifica que el puntero sea de la zona del usuario.
    {
        printk(KERN_ALERT "Read error: puntero usr invalido.\n");
        return -EINVAL;
    }

    *flag_ARDY=0;   //Se limpia flag, "acceso listo".
    *offset_rx=0;   //Offset de puntero de *dato.
    *flag_BF = 2;   //Indico que el recurso no esta libre y ademas que es utilizado por read()
    
    //Configuro el Threshold value
    if(size_usr > 32)
    {   //Si los datos solicitados son mayores a 32B, la FIFO se configura a 32B. Utilizando luego RDR.
        iowrite32(RXTRSH_VALUE(32), HostCtrl.i2c2_reg + I2C_BUF);
    }
    else
    {
        iowrite32(RXTRSH_VALUE(size_usr), HostCtrl.i2c2_reg + I2C_BUF);
    }
    
    //Verifico si es necesario la interrupcion RDR. Solo se utiliza cuando la cantidad de datos a esperar no es un multiplo de la FIFO.
    if( size_usr>32 && (size_usr%32!=0) )
    {
        iowrite32(RDR, HostCtrl.i2c2_reg + I2C_IRQSTATUS);          //Limpio interrupcion.
        iowrite32(RDR_IE, HostCtrl.i2c2_reg + I2C_IRQENABLE_SET);   //Habilito interrupcion.
    }
    else
    {
        iowrite32(RDR_IE, HostCtrl.i2c2_reg + I2C_IRQENABLE_CLR);   //Deshabilito la interrupcion
    }
    
    iowrite32(size_usr, HostCtrl.i2c2_reg + I2C_CNT);               //1) Cantidad de bytes a transferir.
    iowrite32(I2C_EN|MST|STT|STP, HostCtrl.i2c2_reg + I2C_CON);     //2) Iniciar recepción: Master, START, Rx, STOP
    
    //Espera en modo reposo hasta que reciba los datos esperados y el recurso este listo para reutilizarse.
    if( wait_event_interruptible(i2c_waitqueue, (*flag_ARDY==1 && *flag_BF==1) ) !=0 )
    {
        printk(KERN_ALERT"Read error: wait_event_interruptible() interrumpido por una signal. No se cumple ARDY = 1.\n");
        return -ERESTARTSYS;
    }
    
    //Copio datos a la memoria del usuario.
    if(copy_to_user(userbuff, (char *)dato, size_usr)>0)
    {
        printk(KERN_ALERT "Read error: copy_to_user. No se logró copiar todos los datos.\n");
        return -EFAULT;
    }
    
    //printk(KERN_NOTICE "Read: successfully completed.\n");

    return *offset_rx;  //Retorno la cantidad de bytes leidos
}

/****************************************************
 *  Funciones del Platform Dirver                   *
 ****************************************************/
static int probe_function(struct platform_device *pdev)
{    
    printk(KERN_ALERT "Probe: Inicializando I2C...\n");
    
    /* Recursos de Interrupción*/
    HostCtrl.IRQ_BB = platform_get_irq(pdev,0);
    if(HostCtrl.IRQ_BB<0)
    {
        printk(KERN_ALERT"Error: No se pudo generar recurso de interrupcion.\n");
        return -EIO;
    }
    
    HostCtrl.int_i2c = request_irq(HostCtrl.IRQ_BB, (irq_handler_t)i2c_int_handler, IRQF_TRIGGER_RISING, pdev->name, NULL);
    if(HostCtrl.int_i2c!=0)
    {
        printk(KERN_ALERT"Error: No se pudo asociar manejador de interrupcion.\n");
        return HostCtrl.int_i2c;
    }
    
    /* Solicitud de recursos */
    HostCtrl.i2c2_reg = ioremap(I2C2_REG, SIZE_I2C2);
    HostCtrl.cm_per = ioremap(CM_PER_REG, SIZE_CM_PER);
    HostCtrl.control_module = ioremap(CONTROL_MODULE_REG, SIZE_CONTROL_MODULE);
    if( (HostCtrl.i2c2_reg==NULL) || (HostCtrl.cm_per==NULL) || (HostCtrl.control_module==NULL) )
    {
        printk(KERN_ALERT"Error: ioremap(). No se pudo general crear recursos.\n");
        return -ENOMEM;
    }
    
    /*****INICIALIZACION DEL I2C*****/
    iowrite32(0, HostCtrl.i2c2_reg + I2C_CON);      //Deshabilito el I2C
    
    //Configuracion de clock y PADs
    iowrite32(0x2, HostCtrl.cm_per + CM_PER_I2C2_CLKCTRL);  //Habilito I2C clock.
    
    //Slow Slew Rate. Receiver disabled. Pullup/pulldown disabled. Mode 3.
    iowrite32(0x6B, HostCtrl.control_module + UART1_CTSN);  //P9:19 -> SCL
    iowrite32(0x6B, HostCtrl.control_module + UART1_RTSN);  //P9:20 -> SDA
    
    /* inicialización del I2C */
    //Configuración del I2C2
    printk(KERN_NOTICE "Se reinciara el HW I2C...\n");
    iowrite32(SRST, HostCtrl.i2c2_reg + I2C_SYSC);          //Reinicio I2C
    
    udelay(100);    //Espero a que el reinicio se haya completado.

    printk(KERN_NOTICE "Reinicio del I2C finalizado.\n");
    iowrite32(BOTH_CLK|NO_IDLE, HostCtrl.i2c2_reg + I2C_SYSC);  //Configuro: Both clock; No idle.
    
    iowrite32(3, HostCtrl.i2c2_reg + I2C_PSC);      //1)PSC=3 => div = (PSC + 1) => SCLK/div
    iowrite32(53, HostCtrl.i2c2_reg + I2C_SCLL);    //2)SCLL=53 => tLOW = (SCLL + 7)*T0_ICLK = 5uS
    iowrite32(55, HostCtrl.i2c2_reg + I2C_SCLH);    //  SCLH=55 => tHIGH = (SCLH + 5)*T0_ICLK = 5uS
                                                    //Clock para 100kbps
    
    iowrite32(I2C_EN|MST, HostCtrl.i2c2_reg + I2C_CON); //3) Habilito I2C. Mode maestro Normal.
                                                        // I2C_CON:I2C_EN=1 Habilito módulo.
    
    while( (ioread32(HostCtrl.i2c2_reg + I2C_SYSS)&RDONE)==0 ); //Espero a que termine el reinicio.
    
    iowrite32(RRDY|ARDY|RDR|BF, HostCtrl.i2c2_reg + I2C_IRQSTATUS);                 //4) Limpio interrupciones.
    iowrite32(RRDY_IE|ARDY_IE|RDR_IE|BF_IE, HostCtrl.i2c2_reg + I2C_IRQENABLE_SET); //5) Habilito interrupciones.
    
    printk(KERN_NOTICE "Probe: successfully completed.\n");
    
    return 0;
}

static int probe_remove(struct platform_device *pdev)
{
    printk(KERN_NOTICE "Probe remove: Liberando recursos del sistema...\n");
    
    //Deshabilito el I2C2
    iowrite32(0, HostCtrl.i2c2_reg + I2C_CON);
    
    iounmap(HostCtrl.control_module);
    iounmap(HostCtrl.cm_per);
    iounmap(HostCtrl.i2c2_reg);
    
    //Libero recursos de interrupción
    free_irq(HostCtrl.IRQ_BB, NULL);
    
    return 0;
}

/****************************************************
 *  Funciones del Driver                            *
 ****************************************************/
static int __init i2c_init(void)
{
    int return_code;
    
    printk(KERN_NOTICE "Initializing Driver I2C\n");
    
    //Reservo numero mayor y menor de forma dinamica del dispositivo
    return_code = alloc_chrdev_region(&i2c_dev, MINOR_NUM, CANT_DEV, DEVICE_NAME);
    if( return_code < 0 )
    {
        pr_alert("Error al asignar numero menor y mayor.\n");
        return return_code;
    }
    
    pr_notice("Numero mayor: %d\n", MAJOR(i2c_dev));
    pr_notice("Numero menor: %d\n", MINOR(i2c_dev));
    
    //Reservo memoria para la estructura cdev del kernel
    i2c_p_cdev = cdev_alloc();
    if( i2c_p_cdev == NULL )
    {
        printk( KERN_ALERT "No se pudo Reservar memoria cdev.\n" );
        unregister_chrdev_region( i2c_dev, CANT_DEV );
        return -EBUSY;
    }
    
    //Inicializo la estructura de cdev
    i2c_p_cdev->ops = &i2c_ops;         //Se asigna las funciones del driver.
    i2c_p_cdev->owner = THIS_MODULE;    //Registro dueño del dispositivo.
    i2c_p_cdev->dev = i2c_dev;

    //Agrego el cdev al kernel
    return_code = cdev_add( i2c_p_cdev, i2c_dev, CANT_DEV);
    if( return_code < 0 )
    {
        printk( KERN_ALERT "Error al agregar el device driver al kernel.\n" );
        cdev_del(i2c_p_cdev);
        unregister_chrdev_region( i2c_dev, CANT_DEV );
        
        return return_code;
    }
    
    /*Creación de clase e I-nodo*/
    i2c_pclase = class_create(THIS_MODULE, CLASS_NAME);   //Se crea una clase e inodo.
    if(i2c_pclase == NULL)
    {
        printk( KERN_ALERT "Error: no se pudo crear clase (inodo) en el sistema.\n" );
        cdev_del(i2c_p_cdev);   //Elimino el cdev del sistema y libero la memoria
        unregister_chrdev_region( i2c_dev, CANT_DEV );
        return -EBUSY;
    }
    
    //Creo el device dentro de la clase.
    i2c_pclase -> dev_uevent = change_permission_cdev;      //Se asigna los permisos al nuevo idono.
    i2c_pdevice_sys = device_create(i2c_pclase, NULL, i2c_dev , NULL, DEVICE_NAME);
    if( i2c_pdevice_sys  == NULL)
    {
        printk( KERN_ALERT "Error: no se pudo crear el dispositivo.\n");
        class_destroy( i2c_pclase );
        cdev_del(i2c_p_cdev);
        unregister_chrdev_region( i2c_dev, CANT_DEV );
        return -EBUSY;
    }
    
    printk(KERN_NOTICE "Inicializacion del modulo finalizado.\n");

    /* Instanciar planform driver */
    return_code = platform_driver_register(&Mi_I2C_Host_Controller);    //Registro el platform driver en el kernel.
    if( return_code < 0 )
    {
        printk( KERN_ALERT "Error: no se pudo registrar el platform driver.\n");
        device_destroy(i2c_pclase, i2c_dev);
        class_destroy( i2c_pclase );
        cdev_del(i2c_p_cdev);
        unregister_chrdev_region( i2c_dev, CANT_DEV );
        return return_code;
    }

    init_waitqueue_head(&i2c_waitqueue);    //Declaro cola de espera
    
    printk( KERN_NOTICE "Se instalo el driver correctamente.\n");
    
    return 0;
}

static void __exit i2c_exit(void)
{
    platform_driver_unregister(&Mi_I2C_Host_Controller);
    device_destroy(i2c_pclase, i2c_dev);
    class_destroy( i2c_pclase );
    cdev_del(i2c_p_cdev);
    unregister_chrdev_region( i2c_dev, CANT_DEV );
    printk( KERN_ALERT "Se desinstalo el driver.\n");
    
    return;
}

static int change_permission_cdev( struct device *dev, struct kobj_uevent_env *env )
{
    add_uevent_var( env, "DEVMODE=%#o", 0666 ); //Permisos de RW
    
    return 0;
}

static irqreturn_t i2c_int_handler(int irq, void *dev_id, struct pt_regs *regs)
{
    static volatile uint32_t val_reg, size_rx;
    
    val_reg = ioread32(HostCtrl.i2c2_reg + I2C_IRQSTATUS);  //Leo las interrupciones

    if( (val_reg&RDR)!=0 )  //Receive draining
    {
        size_rx = ((ioread32(HostCtrl.i2c2_reg + I2C_BUFSTAT) >> 8) & RXSTAT_MASK); //Leo RXSTAT, para saber cuantos bytes faltan por leer.
        iowrite32(RDR, HostCtrl.i2c2_reg + I2C_IRQSTATUS);                          //Limpio las interrupcion RDR
        
        //Leo los datos restantes de la FIFO RX
        while( size_rx > 0 )
        {
            *( dato + (*offset_rx) ) = (uint8_t)ioread32(HostCtrl.i2c2_reg + I2C_DATA);
            size_rx--;      //Decremento la cantidad de bytes a leer
            (*offset_rx)++; //Incremento el offset
        }
        
    }
    if( (val_reg&ARDY)!=0 ) //Acceso libre.
    {
        wake_up_interruptible(&i2c_waitqueue);
        *flag_ARDY=1;   //Access ready, transfer performed
    }
    if( (val_reg&RRDY)!=0 ) //Recepción lista. Se ha llegado a alcanzar el umbral RX.
    {
        *(dato + (*offset_rx) ) = (uint8_t)ioread32(HostCtrl.i2c2_reg + I2C_DATA);  //Leo dato del buffer Rx
        iowrite32(RRDY, HostCtrl.i2c2_reg + I2C_IRQSTATUS);                         //Limpio las interrupcion RRDY
        size_rx = ((ioread32(HostCtrl.i2c2_reg + I2C_BUF) >> 8) & RXTRSH_MASK);     //Leo RXTRSH, para saber cuantos bytes faltan por leer.
        (*offset_rx)++;     //Incremento el offset
        
        //Leo los datos restantes de la FIFO RX
        while( size_rx > 0 )
        {
            *( dato + (*offset_rx) ) = (uint8_t)ioread32(HostCtrl.i2c2_reg + I2C_DATA);
            size_rx--;      //Decremento la cantidad de bytes a leer
            (*offset_rx)++; //Incremento el offset
        }
    }
    if( (val_reg&BF)!=0 )
    {
        //Si el flag es 0, indica que se esta utilizando write(). De lo contrario esta en uso read(), que no es necesario ejecutar wake_up_interruptible().
        if( *flag_BF == 0 )
            wake_up_interruptible(&i2c_waitqueue);
        *flag_BF = 1;
    }
    
    iowrite32(val_reg, HostCtrl.i2c2_reg + I2C_IRQSTATUS);  //Limpio las interrupciones
    
    return IRQ_HANDLED;
}

module_init(i2c_init);
module_exit(i2c_exit);
