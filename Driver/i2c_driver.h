/************************************************
 *                  INCLUDES                    *
 ************************************************/
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <asm/io.h>
#include <linux/ioport.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/semaphore.h>
#include <linux/spinlock.h>
#include <linux/list.h>
#include <linux/device.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_platform.h>
#include <linux/of_address.h>
#include <linux/ioctl.h>
#include <linux/version.h>

#include "AM335x.h"
#include "ioctl.h"

/************************************************
 *                  DEFINES                     *
 ************************************************/
#define MINOR_NUM       0           //Primer numero menor solicitado
#define CANT_DEV        1           //Cantidad de numeros menores solicitados
#define DRIVER_NAME     "i2c_td3"
#define DEVICE_NAME     "i2c_td3"   //Nombre del Device
#define COMPATIBLE_NAME "td3,omap4-i2c"
#define CLASS_NAME      "td3_class"

/****************************************************
 *  Prototipos                                      *
 ****************************************************/
static ssize_t i2c_write(struct file *file, const char __user *userbuff, size_t size_usr, loff_t *offset);
static ssize_t i2c_read(struct file *file, char __user *userbuff, size_t size_usr, loff_t *offset);
static long i2c_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
static int i2c_open(struct inode *inode, struct file *file);
static int i2c_close(struct inode *inode, struct file * file);

static int probe_remove(struct platform_device *pdev);
static int probe_function(struct platform_device *pdev);

static int change_permission_cdev( struct device *dev, struct kobj_uevent_env *env );

static irqreturn_t i2c_int_handler(int irq, void *dev_id, struct pt_regs *regs);
