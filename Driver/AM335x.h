/**********************************
 * Direcciones de memoria física. *
 **********************************/
#define CM_PER_REG          0x44E00000
#define CONTROL_MODULE_REG  0x44E10000
#define I2C2_REG            0x4819C000

/**********************************
 * Tamaños de registros memoria.  *
 **********************************/
#define SIZE_CM_PER         (1*1024)      //1kB
#define SIZE_I2C2           (4*1024)      //4kB
#define SIZE_CONTROL_MODULE (128*1024)    //128kB

/**********************************
 * Offsets de registros memoria.  *
 **********************************/
#define CM_PER_I2C2_CLKCTRL 0x44
#define UART1_CTSN          0x978
#define UART1_RTSN          0x97C
#define I2C_PSC             0xB0
#define I2C_SCLL            0xB4
#define I2C_SCLH            0xB8

/************** I2C ***************/
//System Configuration Register
#define I2C_SYSC            0x10
#define BOTH_CLK            (3<<8)      //CLKACTIVITY:Both clocks must be kept active.
#define NO_IDLE             (1<<3)      //IDLEMODE: No Idle
#define SRST                (1<<1)      //SoftReset bit.

//I2C Status Raw Register
#define I2C_IRQSTATUS_RAW   0x24
//I2C Status Register
#define I2C_IRQSTATUS  	    0x28
#define RDR                 (1<<13)     //Receive Draining
#define BB                  (1<<12)     //Bus Busy
#define BF             	    (1<<8)		//Bus Free
#define STC                 (1<<6)      //Start Condition
#define GC                  (1<<5)		//General Call
#define XRDY                (1<<4)		//Transmit data ready
#define RRDY	            (1<<3)		//Receive  Data Read
#define ARDY                (1<<2)		//Register Access Ready
#define NACK                (1<<1)      //No Acknowledgment
#define AL                  (1<<0)      //Arbitration Lost

#define I2C_IRQENABLE_SET   0x2C
#define I2C_IRQENABLE_CLR   0x30
#define RDR_IE              (1<<13)     //Receive Draining
#define BF_IE               (1<<8)		//Bus Free Interrupt Enabled
#define XRDY_IE             (1<<4)		//Transmit data ready
#define RRDY_IE             (1<<3)		//Receive  Data Ready
#define ARDY_IE             (1<<2) 		//Register Access Ready Interrupt Enabled

//System Status Register
#define I2C_SYSS            0x90
#define RDONE               (1<<0)      //Reset done bit.  

//I2C_BUF Register
#define I2C_BUF             0x94
#define RXTRSH_VALUE(X)     ((X-1)<<8)  //Threshold value for FIFO buffer in RX mode.
#define RXTRSH_MASK         (0x3F)      //Threshold mask

//I2C_CNT Register
#define I2C_CNT             0x98

//I2C_DATA Register
#define I2C_DATA            0x9C

//I2C Configuration Register
#define I2C_CON             0xA4
#define I2C_EN              (1<<15)     //I2C module enable.
#define MST                 (1<<10)     //Master=1/Slave=0
#define TRX                 (1<<9)      //Transmitter=1/receive=0
#define STP                 (1<<1)      //Stop condition
#define STT                 (1<<0)      //Start condition

//I2C Slave Address Register
#define I2C_SA              0xAC

//I2C_BUFSTAT Register
#define I2C_BUFSTAT         0xC0
#define RXSTAT_MASK         (0x3F)      //RX buffer status mask
