#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h> //se precisa para la funcion write()

#include "ioctl.h"

/**
 * @brief En modo maestro, realiza la lectura de un registro del esclavo.
 * @param i2c_fd:    file descriptor del i2c.
 * @param reg:       Registro a leer.
 * @param *buf:      Buffer donde se cargan los datos leidos.
 * @param size_buf:  Cantidad de bytes a leer.
 * @return    Devuelve un int con la cantidad de bytes leidos, en caso de error devuelve un numero negativo.
 */
int i2c_receive(int i2c_fd, uint8_t reg, uint8_t *buf, uint32_t size_buf);

/**
 * @brief Modo maestro, realiza la escritura en registro del esclavo.
 * @param i2c_fd:    file descriptor del i2c.
 * @param *buf:      Buffer con datos a escribir.
 * @param size_buf:  Cantidad de bytes a escribir.
 * @return    Devuelve un int con la cantidad de bytes transferidos, en caso de error devuelve un numero negativo.
 */
int i2c_transmit(int i2c_fd, uint8_t *buf, uint32_t size_buf);
