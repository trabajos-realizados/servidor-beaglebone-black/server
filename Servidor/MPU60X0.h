/****** REGISTER MAP MPU6000/MPU6050 ******/
#define SELF_TEST_X	0x0D		// R/W
#define SELF_TEST_Y	0x0E		// R/W
#define SELF_TEST_Z	0x0F		// R/W
#define SELF_TEST_A	0x10		// R/W
#define SMPLRT_DIV	0x19		// R/W
#define CONFIG		0x1A		// R/W
#define GYRO_CONFIG	0x1B		// R/W
#define ACCEL_CONFIG	0x1C	// R/W
#define FIFO_EN			0x23	// R/W
#define I2C_MST_CTRL	0x24	// R/W
#define I2C_SLV0_ADDR	0x25	// R/W
#define I2C_SLV0_REG	0x26	// R/W
#define I2C_SLV0_CTRL	0x27	// R/W
#define I2C_SLV1_ADDR	0x28	// R/W
#define I2C_SLV1_REG	0x29	// R/W
#define I2C_SLV1_CTRL	0x2A	// R/W
#define I2C_SLV2_ADDR	0x2B	// R/W
#define I2C_SLV2_REG	0x2C	// R/W
#define I2C_SLV2_CTRL	0x2D	// R/W
#define I2C_SLV3_ADDR	0x2E	// R/W
#define I2C_SLV3_REG	0x2F	// R/W
#define I2C_SLV3_CTRL	0x30	// R/W
#define I2C_SLV4_ADDR	0x31	// R/W
#define I2C_SLV4_REG	0x32	// R/W
#define I2C_SLV4_DO		0x33	// R/W
#define I2C_SLV4_CTRL	0x34	// R/W
#define I2C_SLV4_DI		0x35	// R
#define I2C_MST_STATUS	0x36	// R
#define INT_PIN_CFG		0x37	// R/W
#define INT_ENABLE		0x38	// R/W
#define INT_STATUS		0x3A	// R
#define ACCEL_XOUT_H	0x3B	// R
#define ACCEL_XOUT_L	0x3C	// R
#define ACCEL_YOUT_H	0x3D	// R
#define ACCEL_YOUT_L	0x3E	// R
#define ACCEL_ZOUT_H	0x3F	// R
#define ACCEL_ZOUT_L	0x40	// R
#define TEMP_OUT_H	0x41		// R
#define TEMP_OUT_L	0x42		// R
#define GYRO_XOUT_H	0x43		// R
#define GYRO_XOUT_L	0x44		// R
#define GYRO_YOUT_H	0x45		// R
#define GYRO_YOUT_L	0x46		// R
#define GYRO_ZOUT_H	0x47		// R
#define GYRO_ZOUT_L	0x48		// R

#define I2C_MST_DELAY_CTRL	0x67	// R/W
#define SIGNAL_PATH_RESET	0x68	// R/W
#define USER_CTRL		0x6A	// R/W
#define PWR_MGMT_1		0x6B	// R/W
#define PWR_MGMT_2		0x6C	// R/W
#define FIFO_COUNTH		0x72	// R/W
#define FIFO_COUNTL		0x73	// R/W
#define FIFO_R_W		0x74	// R/W
#define WHO_AM_I		0x75	// R

/*********** SLAVE ADDRESS *************/
#define SLAVE_ADDRESS   0x68

/************* BIT FIELD ***************/
/***************************************/

/*************** FIFO_EN ***************/
#define TEMP_OUT        (1<<7)
#define GYRO_XOUT       (1<<6)
#define GYRO_YOUT       (1<<5)
#define GYRO_ZOUT       (1<<4)
#define ACCEL           (1<<3)

/*************** CONFIG ****************/
#define DLPF_CFG_260HZ  (0<<0)
#define DLPF_CFG_184HZ  (1<<0)
#define DLPF_CFG_94HZ   (2<<0)
#define DLPF_CFG_44HZ   (3<<0)
#define DLPF_CFG_21HZ   (4<<0)
#define DLPF_CFG_10HZ   (5<<0)
#define DLPF_CFG_5HZ    (6<<0)

/************ GYROS CONFIG *************/
#define GYRO_SCALE_250DPS   (0<<3)	// +/- 250 °/s
#define GYRO_SCALE_500DPS   (1<<3)	// +/- 500 °/s
#define GYRO_SCALE_1000DPS  (2<<3)	// +/- 1000 °/s
#define GYRO_SCALE_2000DPS  (3<<3)	// +/- 2000 °/s

/************ ACCEL_CONFIG *************/
#define ACCEL_SCALE_2G      (0<<3)	// +/- 2g
#define ACCEL_SCALE_4G      (1<<3)	// +/- 4g
#define ACCEL_SCALE_8G      (2<<3)	// +/- 8g
#define ACCEL_SCALE_16G     (3<<3)	// +/- 16g

//LSB Sensitivity
#define ACCEL_LSB_2G        16384   //LSB/g
#define ACCEL_LSB_4G        8192    //LSB/g
#define ACCEL_LSB_8G        4096    //LSB/g
#define ACCEL_LSB_16G       2048    //LSB/g

/************* INT_PIN_CFG **************/
#define INT_LEVEL			(1<<7)	//Set 0: active high. Set 1: active low.
#define INT_OPEN            (1<<6)  //Set 0: pin as push-pull. Set 1: pin as open drain.
#define LATCH_INT_EN    	(1<<5)  //Set 0: pin emits a 50us long pulse. 
									//Set 1: pin is hel hig until the interrupt is cleared.
#define INT_RD_CLEAR    	(1<<4)  //Set 0: interrupt status bits are cleared only by reading INT_STATUS.
									//Set 1: interrupt status bits are cleared on any read operation.

/************** INT_ENABLE **************/
#define WOM_EN              (1<<6)  //Enable interrupt for wake on motion to propagate to interrupt pin
#define FIFO_OVERFLOW_EN    (1<<4)  //Enable interrupt for fifo overflow to propagate to interrupt pin.
#define FSYNC_INT_EN        (1<<3)  //Enable Fsync interrupt to propagate to interrupt pin.
#define RAW_RDY_EN          (1<<0)  //Enable Raw Sensor Data Ready interrupt to propagate to interrupt pin

/************** USER_CTRL **************/
#define UC_FIFO_EN     	(1<<6)
#define FIFO_RST        (1<<2)
#define SIG_COND_RST    (1<<0)

/************* PWR_MGMT_1 **************/
#define H_RESET         (1<<7)
#define SLEEP           (1<<6)
#define CYCLE           (1<<5)
#define TEMP_DIS        (1<<3)
#define CLKSEL_20MHz    (0<<0)  //0:Internal 20MHz Osc. 1:Auto selects clock. 

/************* PWR_MGMT_2 **************/
#define STBY_XA      (1<<5)  //X_accel standby mode
#define STBY_YA      (1<<4)  //Y_accel standby mode
#define STBY_ZA      (1<<3)  //Z_accel standby mode
#define STBY_XG      (1<<2)  //X_gyro standby mode
#define STBY_YG      (1<<1)  //Y_gyro standby mode
#define STBY_ZG      (1<<0)  //Z_gyro standby mode
