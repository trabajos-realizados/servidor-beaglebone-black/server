#include "system_functions.h"

/**
 * Realiza un filtrado digital empleando un filtro FIR.
 * @param x        vector con datos de entrada
 * @param x_pass   vector con datos pasados
 * @param y        vector con datos filtrados, corresponde a datos de salida
 * @param len         cantidad de datos de x[]
 * @param pass_len    cantidad de datos de x_pass[]
 * @param *b        el vector de coeficientes del filtro FIR
 * @return          void
 */
void filtro_fir(float x[], float x_pass[], float *y, int len, int pass_len, fir_t *b)
{
    int n, k, top;
    float prod;

    for(n=0; n < len; n++)
    {
        prod = 0;
        top = 1;

        for(k=0; k <= MAX_COEF; k++ )
        {
            if( ((n-k)<0) && ((pass_len-top)>=0) )
            {
                prod += b[k]*x_pass[pass_len-top];
                top++;
            }
            else if( (n-k)>=0 )
            {
                prod += b[k]*x[n-k];
            }
        }
        *(y+n) = prod;
    }
}

/**
 * Lee el archivo config.txt y rellena la estructura de configuracion del servidor.
 * Si no existe lo crea y carga los valores de configuracion por defecto:
 * max_conexiones = 100
 * backlog = 20
 *
 * @param *config Puntero de la estructura de configuracion a rellenar
 * @return  Si rellena la estructura de manera exitosa retorna 0, de lo contrario retorna -1.
 */
int getFileConfig(socketConfig_t *config, fir_t *fir_coef)
{
    FILE *fd_config;
    char buffer[512], campo[15], valor[512];
    int cont;

    fd_config = fopen("./config.txt", "a+");   //Se abre el archivo de configuracion modo RW, si no existe lo crea.
    if( fd_config == NULL )
    {
        perror("Error fopen(): \n");
        return -1;
    }

    //Si el archivo esta vacio se configura con valores por defecto.
    fseek( fd_config, 0, SEEK_END );
    if (ftell( fd_config ) == 0 )
    {
        printf("Archivo de configuracion no encontrado. Se cargaran los valores de configuracion por defecto:\n");

        //Relleno la estructura de configuracion con valores por defecto.
        config->max_connections = 100;
        config->backlog = 20;
        fir_coef[0] = B0;
        fir_coef[1] = B1;
        fir_coef[2] = B2;
        fir_coef[3] = B3;
        fir_coef[4] = B4;
        fir_coef[5] = B5;
        fir_coef[6] = B6;
        fir_coef[7] = B7;
        fir_coef[8] = B8;
        fir_coef[9] = B9;
        fir_coef[10] = B10;

        //Cargo los valores por defecto en el archivo de configuracion.
        rewind(fd_config);  //El puntero de lectura se posiciona al comienzo del archivo.
        fprintf(fd_config, "max_conexiones = 100\nbacklog = 20\nfiltro = ");
        fprintf(fd_config, "%lf", fir_coef[0]);
        for( cont = 1 ; cont <= MAX_COEF; cont++)
            fprintf(fd_config, ",%lf", fir_coef[cont]);
    }
    else    //Si el archivo existe, lo leo y cargo la estructura
    {
        rewind(fd_config);  //El puntero de lectura se posiciona al comienzo del archivo.

        while( fgets(buffer, sizeof(buffer), fd_config) != NULL )  //Leo toda la linea y lo guardo en buffer.
        {
            sscanf(buffer, "%s = %s", campo, valor);

            if(strcmp("max_conexiones", campo)==0)
            {
                config->max_connections = atoi(valor);
            }
            if(strcmp("backlog", campo)==0)
            {
                config->backlog = atoi(valor);
            }
            if(strcmp("filtro", campo)==0)
            {
                cont = 0;
                //filtro = -0.05050399753520547
                while( strchr(valor, ',') != NULL ) //Mientras haya coeficientes extraigo los valores.
                {
                    sscanf(valor, "%f,%s", &(fir_coef[cont]), buffer);
                    strcpy(valor, buffer);
                    cont++;
                }
                sscanf(valor, "%f", &(fir_coef[cont]) );
            }
        }
    }
    printf( "max_conexiones = %d\nbacklog = %d\nfiltro = \n", config->max_connections, config->backlog);
    for( cont = 0 ; cont <= MAX_COEF; cont++)
        printf("    %d: %lf\n", cont, fir_coef[cont]);

    fclose(fd_config);

    return 0;
}
