#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <math.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#include "mpu_api.h"
#include "system_functions.h"

#define PUERTO  4000

#define SEM_VAL 100

/************************
 *  MEMORIA COMPARTIDA  *
 ************************/

typedef struct
{
    int currentConnections;
}shared_memory_t;

typedef struct
{
    float accel_x, accel_y, accel_z, accel_abs;
}mpu6050_reg_t;

/************************
 *      PROTOTIPOS      *
 ************************/
void *proceso_mpu(void *param);
void *procesoHijo(void *socket);
int initSocket(struct sockaddr_in *server, int backlog, int port);
void handlerSIGUSR2(int);
void handlerSIGINT(int);
