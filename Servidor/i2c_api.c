#include "i2c_api.h"

/**
 * @brief En modo maestro, realiza la lectura de un registro del esclavo.
 * @param i2c_fd:    file descriptor del i2c.
 * @param reg:       Registro a leer.
 * @param *buf:      Buffer donde se cargan los datos leidos.
 * @param size_buf:  Cantidad de bytes a leer.
 * @return    Devuelve un int con la cantidad de bytes leidos, en caso de error devuelve un numero negativo.
 */
int i2c_receive(int i2c_fd, uint8_t reg, uint8_t *buf, uint32_t size_buf)
{
    if( write(i2c_fd, (char *)&reg, 1)<0 )      //Indico registro a leer.
    {
        perror("Write error: ");
        return -1;
    }
    
    if( read(i2c_fd, (char *)buf, size_buf)<0 ) //Lectura de registro
    {
        perror("Read error: ");
        return -2;
    }
    
    return size_buf;
}

/**
 * @brief Modo maestro, realiza la escritura en registro del esclavo.
 * @param i2c_fd:    file descriptor del i2c.
 * @param *buf:      Buffer con datos a escribir.
 * @param size_buf:  Cantidad de bytes a escribir.
 * @return    Devuelve un int con la cantidad de bytes transferidos, en caso de error devuelve un numero negativo.
 */
int i2c_transmit(int i2c_fd, uint8_t *buf, uint32_t size_buf)
{    
    if(write(i2c_fd, (char *)buf, size_buf) < 0)
    {
        perror("Write error: ");
        return -1;
    }
    
    if( ioctl(i2c_fd, I2C_STOP, 0)<0 )  //Condición de STOP.
    {
        perror("Ioctl error: I2C_STOP ");
        return -2;
    }
    
    return size_buf;
}
