/* Pendientes:
 */
#include "main.h"

pthread_mutex_t mutex_infoConect, mutex_sock, mutex_fir;

/* MEMORIA COMPARTIDA */
//Usar memoria compartida, emplear semaforos.
shared_memory_t infoConect;
mpu6050_reg_t mpu_raw, mpu_mva;

int sem_id_mpu; //ID para el semaforo
volatile int fd_sock;
volatile int flagSIGINT=0;
uint8_t flag_config=0;

//Estructura del Semaforo
union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO*/
};

int main(int argc, char *argv[])
{    
    union semun sem_arg;
    
    key_t key_sem;
    int fd_cliente, return_state=0;
    socklen_t longClient;
    struct sockaddr_in server, client;
    socketConfig_t config;
    fir_t fir_coef[MAX_COEF+1];
    pthread_t thread_id, thread_id_mpu;
    
    if( getFileConfig(&config, fir_coef)== -1)  //Relleno la estructura de configuracion.
    {
        printf("Error getFileConfig\n");
        return -1;
    }

    fd_sock = initSocket(&server, config.backlog, PUERTO);  //Inicializo socket

    if( pthread_mutex_init(&mutex_infoConect, NULL)==-1 )   //Creo mutex e inicializo como unlock
    {
        perror("Error pthread_mutex_init: ");
        close(fd_sock);                             //Cierro socket
        return -1;
    }
    if( pthread_mutex_init(&mutex_sock, NULL)==-1 ) //Creo mutex e inicializo como unlock
    {
        perror("Error pthread_mutex_init: ");
        close(fd_sock);                             //Cierro socket
        pthread_mutex_destroy(&mutex_infoConect);   //Elimino mutex
        return -1;
    }
    if( pthread_mutex_init(&mutex_fir, NULL)==-1 )  //Creo mutex e inicializo como unlock
    {
        perror("Error pthread_mutex_init: ");
        close(fd_sock);                             //Cierro socket
        pthread_mutex_destroy(&mutex_infoConect);   //Elimino mutex
        pthread_mutex_destroy(&mutex_sock);
        return -1;
    }

    //Inicializacion del recurso de Semaforos.
    key_sem = ftok("./Servidor/main.c", 's');   //Creo clave para Semaforos
    if( key_sem == -1 )
    {
        perror("Error fork():");
        return -1;
    }

    sem_id_mpu = semget(key_sem, 1, 0666|IPC_CREAT);    //Creo Semaforo
    if(sem_id_mpu==-1)
    {
        perror("Error semget: ");
        return -1;
    }

    //Creo un semaforo y lo inicializo segun INIT_SEM
    sem_arg.val=SEM_VAL;
    if( semctl(sem_id_mpu, 0, SETVAL, sem_arg ) == -1 )
    {
        perror("Error semtcl: ");
        return -1;
    }
    
    //Inicializo signals
    signal(SIGUSR2, handlerSIGUSR2);
    signal(SIGINT, handlerSIGINT);

    //Inicializo recursos compartidos
    infoConect.currentConnections = 0;
    flag_config = 0;

    //Creo nuevo hilo de ejecucion, el cual se encargara de interactuar con el MPU6050.
    if( pthread_create( &thread_id_mpu, NULL, proceso_mpu, (void*)fir_coef) != 0)
    {
        perror("Error pthread_create: ");   //Ante un error rompo el blucle y cierro el servidor.
        return -1;
    }
    
    printf("Esperando conexiones... \n");

    while(1)
    {
        if( flagSIGINT == 1 )
        {
            printf("Cerrando servidor\n");
            break;
        }

        //Antes de aceptar conexiones, verifico que no supere la cantidad maxima de conexiones permitidas.
        if( pthread_mutex_lock(&mutex_infoConect)==-1 ) //Tomo el mutex
        {
            perror("Error pthread_mutex_lock: ");       //Ante un error rompo el blucle y cierro el servidor.
            return_state=-1;
            break;
        }

        //Verifico si hay actulizaciones del sistema
        if( flag_config == 1)
        {
            if( pthread_mutex_lock(&mutex_fir)==-1 )    //Tomo el mutex para actualizar los coeficiones del FIR
            {
                perror("Error pthread_mutex_lock: ");   //Ante un error rompo el blucle y cierro el servidor.
                return_state=-1;
                break;
            }

            flag_config = 0;    //Limpio bandera
            if( getFileConfig(&config, fir_coef) ==-1)  //Obtengo los datos de configuracion del archivo.
            {
                printf("Error getFileConfig\n");
                break;
            }

            if( pthread_mutex_unlock(&mutex_fir)==-1 )      //Libero el mutex FIR
            {
                perror("Error pthread_mutex_unlock: ");     //Ante un error rompo el blucle y cierro el servidor.
                return_state=-1;
                break;
            }
        }

        if( infoConect.currentConnections >= config.max_connections )
        {
            pthread_mutex_unlock(&mutex_infoConect);    //Libero el mutex
            sleep(1);   //Modo reposo por 1 segundo
            continue;   //Regreso al while(1) a consultar si hay disponibilidad de mas conexiones.
        }
        if( pthread_mutex_unlock(&mutex_infoConect)==-1 )   //Libero el mutex
        {
            perror("Error pthread_mutex_unlock: ");         //Ante un error rompo el blucle y cierro el servidor.
            return_state=-1;
            break;
        }

        //Aceptar conexiones
        //Si el thread esta utilizando el mutex, dado que el mismo esta realizando una copia de fd_cliente,
        //se bloquea hasta que el thread lo libere.
        if( pthread_mutex_lock(&mutex_sock)==-1 ) //Tomo el mutex SOCK
        {
            perror("Error pthread_mutex_lock: ");       //Ante un error rompo el blucle y cierro el servidor.
            return_state=-1;
            break;
        }

        longClient = sizeof(client);
        fd_cliente = accept(fd_sock, (struct sockaddr *)&client, &longClient);
        if( fd_cliente == -1 )
        {
            perror( "Error accept: " ); //Ante un error rompo el blucle y cierro el servidor.
            return_state=-1;
            break;
        }

        if( pthread_mutex_unlock(&mutex_sock)==-1 )     //Libero el mutex   SOCK
        {
            perror("Error pthread_mutex_unlock: ");     //Ante un error rompo el blucle y cierro el servidor.
            return_state=-1;
            break;
        }

        printf("Nuevo cliente: id %d\n", fd_cliente);

        //Creo nuevo hilo de ejecucion.
        if( pthread_create( &thread_id, NULL, procesoHijo, (void *)&fd_cliente) != 0)
        {
            perror("Error pthread_create: ");   //Ante un error rompo el blucle y cierro el servidor.
            return_state=-1;
            break;
        }

        //Doy un tiempo para que el proceso tome el fd del cliente creado
        sleep(1);

        if( pthread_mutex_lock(&mutex_infoConect)==-1 )     //Tomo el mutex
        {
            printf("Error pthread_mutex_lock\n");           //Ante un error rompo el blucle y cierro el servidor.
            return_state = -1;
            break;
        }
        infoConect.currentConnections++;                    //Incremento la cantidad de conexiones actuales
        if( pthread_mutex_unlock(&mutex_infoConect)==-1 )   //Libero el mutex
        {
            printf("Error pthread_mutex_unlock\n");         //Ante un error rompo el blucle y cierro el servidor.
            return_state = -1;
            break;
        }
    }

    close(fd_sock);                             //Cierro socket
    pthread_mutex_destroy(&mutex_infoConect);   //Elimino mutex
    pthread_mutex_destroy(&mutex_sock);
    pthread_mutex_destroy(&mutex_fir);
    
    semctl(sem_id_mpu, 0, IPC_RMID, sem_arg);   //Elimino el semaforo
    
    pthread_join( thread_id_mpu, NULL);     //Espero que finalice el proceso_mpu

    printf("Servidor cerrado.\n");

    return return_state;
}

void handlerSIGUSR2(int signal)
{
    printf("Se actualizan los valores de configuracion del servidor.\n");
    flag_config = 1;    //Indico que hay que actualizar los valores de configuracion del sistema.
}

void handlerSIGINT(int signal)
{
    printf("\nSe preciono CTRL+C\nCerrando programa...\n");
    close(fd_sock);
    flagSIGINT = 1;
}

int initSocket(struct sockaddr_in *server, int backlog, int port)
{
    int fd_sock;

    //Configuracion del servidor
    server->sin_family= AF_INET;        //Familia TCP/IP
    server->sin_port = htons(PUERTO);   //Puerto
    server->sin_addr.s_addr = htonl(INADDR_ANY); //Cualquier cliente puede conectarse
    bzero(&(server->sin_zero),8);       //Funcion que rellena con 0's

    printf("##########################\n");
    printf("### DATOS DEL SERVIDOR ###\n");
    printf("### IP: 192.168.0.251  ###\n");
    printf("### Puerto: %d      ###\n", port);
    printf("##########################\n");

    //Creacion de Socket
    fd_sock = socket( AF_INET, SOCK_STREAM, 0 ); //Protocolo TCP
    if( fd_sock < 0 )
    {
        perror("Error al crear Socket.\n");
        exit(0);
    }

    //Asociar una direccion al socket
    if( bind( fd_sock, (struct sockaddr*)server, sizeof(*server) ) < 0)
    {
        perror("Error al asociar el Socket.\n");
        exit(0);
    }

    //Esperar conexiones
    if( listen( fd_sock, backlog ) == -1 )
    {
        perror("Error con listen() Socket.\n");
        exit(0);
    }

    return fd_sock;
}

void *procesoHijo(void *socket)
{
    int *fd_socket, fd_cliente, recv_bytes;
    char buffer[512];
    float gyro_aux=0;
    struct sembuf sem_op = {0, 0, 0};

    if( pthread_mutex_lock(&mutex_sock) == -1)  //Tomo el mutex para leer los datos del nuevo cliente.
    {
        perror("Error pthread_mutex_lock: ");   //Ante un error, cierro el cliente
        close((int)(*(int*)socket));
        pthread_mutex_lock(&mutex_infoConect);
        infoConect.currentConnections--;        //Decremento la cantidad de conexiones actuales
        pthread_mutex_unlock(&mutex_infoConect);
        return NULL;
    }

    fd_socket = (int*)socket;       //Guardo id socket del cliente.
    fd_cliente = *(fd_socket);

    if( pthread_mutex_unlock(&mutex_sock) == -1)  //Libero el mutex.
    {
        perror("Error pthread_mutex_unlock: ");
        close(fd_cliente);
        pthread_mutex_lock(&mutex_infoConect);
        infoConect.currentConnections--;        //Decremento la cantidad de conexiones actuales
        pthread_mutex_unlock(&mutex_infoConect);
        return NULL;
    }

    pthread_detach(pthread_self()); //Desvinculo el proceso del proceso padre.

    printf("Estableciendo comunicacion con cliente %d\n", fd_cliente);

    //Envio OK al cliente
    sprintf(buffer,"OK");
    if( send(fd_cliente, buffer, strlen(buffer), 0) == -1 )
    {
        perror("Error al enviar datos.\n");
        close(fd_cliente);
        pthread_mutex_lock(&mutex_infoConect);
        infoConect.currentConnections--;        //Decremento la cantidad de conexiones actuales
        pthread_mutex_unlock(&mutex_infoConect);
        return NULL;
    }

    printf("Esperando AKN\n");

    //Espero el AKN del cliente
    recv_bytes = recv(fd_cliente, buffer, sizeof(buffer), 0);
    if( recv_bytes < 0 )
    {
        perror("Recv socket failed");
        close(fd_cliente);
        pthread_mutex_lock(&mutex_infoConect);
        infoConect.currentConnections--;        //Decremento la cantidad de conexiones actuales
        pthread_mutex_unlock(&mutex_infoConect);
        return NULL;
    }

    buffer[recv_bytes]='\0';
    printf("Recepcrion del cliente: %s\n", buffer);

    //Si efectivamente recibo AKN continuo, de lo contrario implica un error en la conexion
    if( strcmp(buffer, "AKN") != 0 )
    {
        printf("Hubo un error en la conexion con el cliente\n");
        close(fd_cliente);
        pthread_mutex_lock(&mutex_infoConect);
        infoConect.currentConnections--;        //Decremento la cantidad de conexiones actuales
        pthread_mutex_unlock(&mutex_infoConect);
        return NULL;
    }

    printf("Envio OK\n");
    //Tras recibir el AKN, vuelvo a enviar OK para iniciar el envio de datos del sensor
    sprintf(buffer,"OK");
    if( send(fd_cliente, buffer, strlen(buffer), 0) == -1 )
    {
        perror("Error al enviar datos.\n");
        close(fd_cliente);
        pthread_mutex_lock(&mutex_infoConect);
        infoConect.currentConnections--;        //Decremento la cantidad de conexiones actuales
        pthread_mutex_unlock(&mutex_infoConect);
        return NULL;
    }

    printf("Comunicacion estableciada!!\n");

    while(1)
    {
        //Espero el comando KA para enviar datos al cliente
        recv_bytes = recv(fd_cliente, buffer, sizeof(buffer), 0);
        if( recv_bytes < 0 )
        {
            perror("Recv socket failed");
            break;
        }

        buffer[recv_bytes]='\0';
        printf("Recepcrion del cliente: %s\n", buffer);

        //Cuando reciba el comando KA envio los datos del sensor, de contrario implica un error por lo que se procede a cerrar el cliente.
        if( strcmp(buffer, "END") == 0 )
        {
            printf("El cliente cerro la conexion.\n");
            break;
        }
        if( strcmp(buffer, "KA") != 0 )
        {
            printf("Hubo un error en la conexion con el cliente\n");
            break;
        }

        //Tomo el semaforo para realizar la lectura de la memoria compartida (mpu_raw y mpu_mva).
        sem_op.sem_op = -1;
        if( semop(sem_id_mpu, &sem_op, 1) == -1 )
        {
            perror("Error semop en procesoHijo: ");
            break;
        }
        
        //Armo el string con la informacion necesaria
        sprintf(buffer, "%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n%.5f\n",
                mpu_raw.accel_x, mpu_raw.accel_y, mpu_raw.accel_z, gyro_aux, gyro_aux, gyro_aux, mpu_raw.accel_abs,
                mpu_mva.accel_x, mpu_mva.accel_y, mpu_mva.accel_z, gyro_aux, gyro_aux, gyro_aux, mpu_mva.accel_abs);

        printf("### Paquete: %s\n", buffer);
        
        //libero el semaforo
        sem_op.sem_op = 1;
        if( semop(sem_id_mpu, &sem_op, 1) == -1 )
        {
            perror("Error semop en procesoHijo: ");
            break;
        }

        //Envio el buffer de datos MPU
        if( send(fd_cliente, buffer, strlen(buffer), 0) == -1 )
        {
            perror("Error al enviar datos.\n");
            break;
        }
        printf("Cantidad de datos enviados: %d\n", (int)strlen(buffer));
    }

    close(fd_cliente);

    if( pthread_mutex_lock(&mutex_infoConect)==-1 )     //Tomo mutex
    {
        perror("Error pthread_mutex_lock: ");
    }
    infoConect.currentConnections--;                    //Decremento la cantidad de conexiones actuales.
    if( pthread_mutex_unlock(&mutex_infoConect)==-1 )   //Libero el mutex
    {
        perror("Error pthread_mutex_lock: ");
        return NULL;
    }

    printf("### Cliente eliminado ###\n");

    return NULL;
}


void *proceso_mpu(void *param)
{
    fir_t *fir;
    int fd_i2c, fifo_count, xcount, count_pass=0;
    struct MPU6050_REGS mpu, mpu_offset;
    float mpu_abs[1024], mpu_fir[1024], mpu_pass[1024] = {0}, mpu_fir2[1024], mpu_pass2[1024] = {0};
    uint8_t buf[1024];
    struct sembuf sem_op = {0, 0, 0};

    printf("### Proceso MPU ###\n");

    //pthread_detach(pthread_self()); //Desvinculo el proceso del proceso padre.

    fd_i2c = open("/dev/i2c_td3", O_RDWR);
    if( fd_i2c < 0 )
    {
        printf("Error open i2c\n");
        flagSIGINT = 1;
        return NULL;
    }

    if( init_MPU(fd_i2c, ACCEL_SCALE, DLPF_CFG) < 0)
    {
        printf("Init error \n");
        close(fd_i2c);
        flagSIGINT = 1;
        return NULL;
    }

    usleep(500000);   //Espero 0.5 segundos para que MPU se estabilice
    
    if(offset(fd_i2c, &mpu_offset)<0)
    {
        printf("offset() failed\n");
        close(fd_i2c);
        flagSIGINT = 1;
        return NULL;
    }
    
    //Tomo el puntero que contiene el los coeficientes del FIR
    fir = (fir_t*)param;

    while(1)
    {
        //Si se interrumpe el programa con CTRL+C, se rompe el while, se cierran los recursos y se termina el proceso.
        if( flagSIGINT==1 )
            break;

        //Leo los datos de la FIFO. Si no hay datos regreso al while()
        fifo_count = getAccelerometerFIFO(fd_i2c, buf, 0);
        if( fifo_count < 0 )
        {
            printf("getAccelerometerFIFO failed \n");
            break;
        }
        else if (fifo_count == 0)
            continue;

        //De los datos leidos, realizo la conversion y obtengo el valor absoluto
        for( xcount=0; xcount < fifo_count/6; xcount++)
        {
            //Obtengo el valor convertido del acelerometro.
            mpu.accel_x = (int16_t)(( buf[xcount*6]<<8 | buf[xcount*6+1] ) - mpu_offset.accel_x);
            mpu.accel_y = (int16_t)(( buf[xcount*6+2]<<8 | buf[xcount*6+3] ) - mpu_offset.accel_y);
            mpu.accel_z = (int16_t)(( buf[xcount*6+4]<<8 | buf[xcount*6+5] ) - mpu_offset.accel_z);

            //Valor absoluto del acelerometro.
            mpu_abs[xcount] = sqrtf( powf((float)mpu.accel_x, 2) + powf((float)mpu.accel_y, 2) + powf((float)mpu.accel_z, 2) );
        }

        //Tomo el mutex, dado que se leeran sus coeficientes. Para no colisionar con la actualizacion de parametros con SIGUSR2.
        if( pthread_mutex_lock(&mutex_fir)==-1 )
        {
            perror("Error pthread_mutex_lock ");    //Ante un error rompo el blucle y cierro.
            break;
        }
        
        //Aplico el filtro FIR. En la primera ejecucion count_pass=0 y count_pass2=0.

        filtro_fir(mpu_abs, mpu_pass, mpu_fir, xcount, count_pass, fir);
        
        filtro_fir(mpu_fir, mpu_pass2, mpu_fir2, xcount, count_pass, fir);
        
        if( pthread_mutex_unlock(&mutex_fir)==-1 )  //Libero el mutex
        {
            perror("Error pthread_mutex_unlock ");  //Ante un error rompo el blucle y cierro.
            break;
        }

        //Asigno el ultimo valor medido en la memoria compartida

        //Tomo el semaforo completo para escribir
        sem_op.sem_op = -SEM_VAL;
        if( semop(sem_id_mpu, &sem_op, 1) == -1 )
        {
            perror("Error semop en proceso_mpu: ");
            break;
        }

        //Asigno el ultimo dato a la variable compartida, de manera de enviar el dato mas actual al cliente
        //Y ademas, convierto a [g]
        mpu_raw.accel_x = mpu.accel_x/ACCEL_SENSITIVITY;
        mpu_raw.accel_y = mpu.accel_y/ACCEL_SENSITIVITY;
        mpu_raw.accel_z = mpu.accel_z/ACCEL_SENSITIVITY;
        mpu_raw.accel_abs = mpu_abs[xcount-1]/ACCEL_SENSITIVITY;
        mpu_mva.accel_x = fabs((float)mpu.accel_x)/ACCEL_SENSITIVITY;
        mpu_mva.accel_y = fabs((float)mpu.accel_y)/ACCEL_SENSITIVITY;
        mpu_mva.accel_z = fabs((float)mpu.accel_z)/ACCEL_SENSITIVITY;
        mpu_mva.accel_abs = mpu_fir2[xcount-1]/ACCEL_SENSITIVITY;

        //Libero el semaforo completo
        sem_op.sem_op = SEM_VAL;
        if( semop(sem_id_mpu, &sem_op, 1) == -1 )
        {
            perror("Error semop en proceso_mpu: ");
            break;
        }

        //Los valores del mpu actual se convierten en datos del pasado.
        count_pass = xcount;
        memcpy((void *)mpu_pass, (void *)mpu_fir, count_pass*sizeof(float));
        
        memcpy((void *)mpu_pass2, (void *)mpu_fir2, count_pass*sizeof(float));
        
        usleep(100000);
    }

    close(fd_i2c);

    printf("### Proceso MPU finalizado ###\n");

    flagSIGINT = 1;
    
    pthread_exit(NULL);
}
