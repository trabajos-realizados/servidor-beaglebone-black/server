#include "mpu_api.h"

/**
 * @brief una cantidad de muestras y determina el offset presente en el sensor.
 * @param fd                      FD del i2c
 * @param *mpu    Puntero a la estructura donde se guardara el offset correspondiente a cada eje.
 * @return                      Retorna la cantidad de muestras tomadas en caso de exito, de lo contrario devuelve un numero negativo.
 */
int offset(int fd, struct MPU6050_REGS *mpu)
{
    int i, muestras=0, prom_x=0, prom_y=0, prom_z=0, count;
    uint8_t buf[1024];

    while( muestras < 1024 )
    {
        count = getAccelerometerFIFO(fd, buf, 0);
        if( count < 0 )
        {
            printf("getAccelerometerFIFO failed\n");
            return -1;
        }
        else if( count == 0 )    //Si no hay datos en la FIFO regreso al While()
        {
            continue;
        }

        for( i=0; i < count/6; i++)
        {
            mpu->accel_x = (int16_t)( buf[i*6]<<8 | buf[i*6+1] );
            mpu->accel_y = (int16_t)( buf[i*6+2]<<8 | buf[i*6+3] );
            mpu->accel_z = (int16_t)( buf[i*6+4]<<8 | buf[i*6+5] );
            prom_x+=(int)mpu->accel_x;
            prom_y+=(int)mpu->accel_y;
            prom_z+=(int)mpu->accel_z;
            muestras++;
        }
        usleep(1000);
    }
    mpu->accel_x = prom_x/muestras;
    mpu->accel_y = prom_y/muestras;
    mpu->accel_z = prom_z/muestras;

    return muestras;
}

/**
 * Toma datos del acelerometro X, Y e Z
 * @param *mpu:   estructura donde se guardan los datos leidos
 * @return Retorna 0 en caso de exito, -1 en caso de error.
 */
int getAccelerometer(int fd, struct MPU6050_REGS *mpu)
{
    uint8_t data[6];

    //Leo datos accelerometer x, y and z out
    if( i2c_receive(fd, ACCEL_XOUT_H, data, 6) < 0)
    {
        printf("i2c receive error in getAccelerometer()\n");
        return -1;
    }
    mpu->accel_x = (int16_t)( data[0]<<8 | data[1] );
    mpu->accel_y = (int16_t)( data[2]<<8 | data[3] );
    mpu->accel_z = (int16_t)( data[4]<<8 | data[5] );

    return 0;
}

/**
 * Retorna la cantidad de datos almacenados en la FIFO
 * @param fd:     FD del i2c utilizado.
 * @return     Cantidad de datos en la FIFO no leidos. En caso de error retorna -1.
 */
int getFIFO_count(int fd)
{
    uint8_t buf[2];
    int res;

    if( i2c_receive(fd, FIFO_COUNTH, buf, 2) < 0)
    {
        printf("i2c receive error in getFIFO_count\n");

        return -1;
    }

    res = (int)( (buf[0]<<8) | buf[1] );

    return res;
}

/**
 * Extrae los datos de la FIFO segun se indica.
 * @param fd          FD del i2c a utilizar.
 * @param *buf    Puntero donde se cargaran los datos solicitados.
 * @param count  Cantidad de bytes solicitados a extraer de la FIFO. Con 0 se extraen todos los datos disponibles actualmente.
 * @return          En caso de exito retorna la cantidad de bytes leidos exitosamente, de lo contrario retorna -1.
 */
int getAccelerometerFIFO(int fd, uint8_t *buf, uint16_t count)
{
    //Si se indica count=0, se extraen todos los datos de la FIFO del momento.
    if( count == 0 )
    {
        count = getFIFO_count(fd);
        if( count == 0 )
            return 0;
    }

    if( i2c_receive(fd, FIFO_R_W, buf, (uint32_t)count) < 0)
    {
        printf("getAccelerometerFIFO error with data receive.\n");
        return -1;
    }

    return count;
}

//Escala del acelerometro
int init_MPU(int fd, uint8_t scale, uint8_t filter)
{
    uint8_t buf[2];

    /* Configuro dirección del esclavo */
    if( ioctl(fd, I2C_SLAVE, SLAVE_ADDRESS)<0 )
    {
        perror("Error: ioctl() con SLAVE_ADDRESS\n");
        return -1;
    }

    if( i2c_receive(fd, WHO_AM_I, buf, 1) <0 )
    {
        printf("i2c receive error on init_MPU\n");
        return -1;
    }
    printf("Iniciando configuración del sensor %02X\n", buf[0]);

    /* Configuro MPU60X0 */

    //Reinicio MPU
    buf[0] = PWR_MGMT_1;
    buf[1] = H_RESET;
    if( i2c_transmit(fd, buf, 2)<0 )
    {
        printf("Error: i2c_transmit(), reset.\n");
        return -1;
    }

    usleep(100000); //Wait 100ms

    //Habilito clock 20MHz y deshabilito el sensor de temperatura
    buf[0] = PWR_MGMT_1;
    buf[1] = CLKSEL_20MHz|TEMP_DIS;
    if( i2c_transmit(fd, buf, 2)<0 )
    {
        printf("Error: i2c_transmit(), setup PWR_MGMT_1.\n");
        return -1;
    }

    //Reinicio la FIFO y los paths
    buf[0] = USER_CTRL;
    buf[1] = FIFO_RST|SIG_COND_RST;
    if( i2c_transmit(fd, buf, 2)<0 )        //Habilito y reinicio FIFO
    {
        printf("Error: i2c_transmit, habilitación de la FIFO.\n");
        return -1;
    }

    usleep(10000);  //Demora hasta que se reinicie.

    //Habilito la FIFO
    buf[0] = USER_CTRL;
    buf[1] = UC_FIFO_EN;
    if( i2c_transmit(fd, buf, 2)<0 )
    {
        printf("Error: i2c_transmit, habilitación de la FIFO.\n");
        return -1;
    }

    //Habilito la FIFO para el Acelerometro.
    buf[0] = FIFO_EN;
    buf[1] = ACCEL;
    if( i2c_transmit(fd, buf, 2)<0 )
    {
        printf("Error al configurar FIFO.\n");
        return -1;
    }

    /* Configuracion del Sample Rate (SR)
     * Indica la frecuencia en la que los registros son actualizados con los valores medidos.
     * Esto incluye la frecuencia en la que los datos cargados en la FIFO. Por lo que se busca que
     * el sample_rate = 1kHz
     *
     * Para DLPF = 0 -> fs = 8kHz
     * Para DLPF != 0 -> fs = 1kHz
     *
     */

    //División de sample_rate from the gyroscope. SR = 1kH
    buf[0] = SMPLRT_DIV;
    buf[1] = 7;
    if( i2c_transmit(fd, buf, 2)<0 )
    {
        printf("Error: división de sample_rate (SMPLRT_DIV).\n");
        return -1;
    }

    //Configuro el filtro digital. DLPF = 0. Fs=8kHz
    buf[0] = CONFIG;
    buf[1] = filter;
    if( i2c_transmit(fd, buf, 2)<0 )
    {
        printf("Error: configuración general.\n");
        return -1;
    }

    //Escala del acelerometro +/- 2G
    buf[0] = ACCEL_CONFIG;
    buf[1] = scale;
    if( i2c_transmit(fd, buf, 2)<0 )
    {
        printf("Error: configuración del acelerometro.\n");
        return -1;
    }

    //Habilito el accelerometro
    buf[0] = PWR_MGMT_2;
    buf[1] = STBY_XG|STBY_YG|STBY_ZG;
    if( i2c_transmit(fd, buf, 2)<0 )
    {
        printf("Error: configuración del acelerometro.\n");
        return -1;
    }

    return 0;
}
