#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define MAX_COEF    (10)

//Bandpass-FIR-Equiripple	0.1/0.2-0.3/0.4
#define B0  -4.289498049688105e-17
#define B1  0.059930824233648906
#define B2  -3.532527805625498e-17
#define B3  -0.29928474013347905
#define B4  1.261617073437678e-18
#define B5	0.38032304205008494
#define B6	1.261617073437678e-18
#define B7  -0.29928474013347905
#define B8  -3.532527805625498e-17
#define B9  0.059930824233648906
#define B10 -4.289498049688105e-17

//Estructura de configuracion del servidor
typedef struct
{
    int max_connections;
    int backlog;
}socketConfig_t;

typedef float fir_t;

void filtro_fir(float x[], float x_pass[], float *y, int len, int pass_len, fir_t *b);

int getFileConfig(socketConfig_t *config, fir_t *fir_coef);
