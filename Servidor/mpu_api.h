#include <stdio.h>
#include <stdint.h>

#include "i2c_api.h"
#include "MPU60X0.h"

//Parametros de configuracion del sensor
//NOTA: si se cambia el parametro ACCEL_SENSITIVITY, debe cambiarse a su vez ACCEL_SCALE y viceversa.
#define ACCEL_SENSITIVITY   ACCEL_LSB_16G        //Sensibilidad
#define ACCEL_SCALE         ACCEL_SCALE_16G      //Escala
#define DLPF_CFG            DLPF_CFG_21HZ          //Filtro digital

struct MPU6050_REGS
{
    int16_t accel_x, accel_y, accel_z;
};

/**
 * @brief una cantidad de muestras y determina el offset presente en el sensor.
 * @param fd                      FD del i2c
 * @param *mpu    Puntero a la estructura donde se guardara el offset correspondiente a cada eje.
 * @return                      Retorna la cantidad de muestras tomadas en caso de exito, de lo contrario devuelve un numero negativo.
 */
int offset(int fd, struct MPU6050_REGS *mpu);

/**
 * Toma datos del acelerometro X, Y e Z
 * @param *mpu:   estructura donde se guardan los datos leidos
 * @return Retorna 0 en caso de exito, -1 en caso de error.
 */
int getAccelerometerFIFO(int fd, uint8_t *buf, uint16_t count);

/**
 * Retorna la cantidad de datos almacenados en la FIFO
 * @param fd:     FD del i2c utilizado.
 * @return     Cantidad de datos en la FIFO no leidos. En caso de error retorna -1.
 */
int getFIFO_count(int fd);

/**
 * Extrae los datos de la FIFO segun se indica.
 * @param fd          FD del i2c a utilizar.
 * @param *buf    Puntero donde se cargaran los datos solicitados.
 * @param count  Cantidad de bytes solicitados a extraer de la FIFO. Con 0 se extraen todos los datos disponibles actualmente.
 * @return          En caso de exito retorna la cantidad de bytes leidos exitosamente, de lo contrario retorna -1.
 */
int getAccelerometer(int fd, struct MPU6050_REGS *mpu);

/*  Inicializa el MPU de manera de utilizar el accelerometro
 *
 */
int init_MPU(int fd, uint8_t scale, uint8_t filter);
