DRIVER:= i2c_driver
obj-m := $(DRIVER).o

MAIN = main
TEST = main_test_mpu
SRC := $(*.c)
OBJ := $(*.o)
CC = gcc
CFLAGS = -Wall
INC = -lm -pthread

KERNELDIR := /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)

all:
	@echo "Compilando"
	$(CC) $(CFLAGS) -Ofast -c -S ./Servidor/*.c -pthread
	$(CC) $(CFLAGS) -c ./Servidor/*.c -pthread
	$(CC) $(CFLAGS) -o $(MAIN) *.o $(INC)
	./$(MAIN)
	
driver:
	make -C $(KERNELDIR) M=$(PWD)/Driver
	
login:
	ssh debian@192.168.7.2

off:
	@echo "Apagando Beaglebone..."
	sudo shutdown -h now
	
send:
	sudo scp -r Servidor/ Driver/ Test/ Makefile debian@192.168.7.2:/home/debian/tp
	@echo "Archivos enviados a la beagle."
	
mjs:
	sudo dmesg
	
log:
	sudo dmesg -wH

clog:
	sudo dmesg -C
	
install: driver
	@echo "Instalando driver..."
	sudo insmod ./Driver/$(DRIVER).ko
	@echo "Driver instalado!"
	
remove: clean
	@echo "Desisntalando driver..."
	sudo rmmod $(DRIVER)
	
test:
	@echo "Compilando programa de prueba."
	gcc ./Test/$(TEST).c -o $(TEST)
	./$(TEST)

help:
	@echo "### EJECUCION ###"
	@echo "all:	Compila y ejecuta el programa"
	@echo ""
	@echo "### INSTALACION ###"
	@echo "install: Compila e Instala el driver module en el Kernel."
	@echo ""
	@echo "### DESINSTALACION ###"
	@echo "remove:	Desisntalar el driver module del Kernel."
	@echo ""
	@echo "### TEST DEVICE ###"
	@echo "test:	Ejecuta un proceso que solo abre el device i2c_td3. Luego lo cierra si logra abrirlo."
	@echo ""
	@echo "### CONTROL ###"
	@echo "login:	Ingresa a la BBB."
	@echo "off:	Apagar de manera segura la BBB."
	@echo "send:	Enviar archivos a la BBB."
	@echo "mnj:	Log del sistema actual."
	@echo "log:	Log del sistema en tiempo de ejecucion."
	@echo "clog:	Limpia el log del sistema."
	@echo "clean:	Elimina los archivos generados"
clean:
	rm -rf ./Driver/*.o ./Driver/core ./Driver/.depend ./Driver/.*.cmd ./Driver/*.mod.c ./Driver/*.mod ./Driver/*.ko ./Driver/.tmp_versions ./Driver/modules.* ./Driver/Module* ./Driver/*.dwo
	rm -rf *.o core .depend .*.cmd *.mod.c *.mod *.ko .tmp_versions modules.* Module* *.dwo $(MAIN) *.s
